## Running

This project requires Java 8 and Maven to be installed.

Developed and tested with: 

 * Java version 1.8.0_144
 * Apache Maven 3.0.5
 * Windows 10

After cloning, application can be run by typing on the command line in the project's root directory:

	mvn spring-boot:run
	
Data is loaded into an in-memory H2 database at startup so no further software is required.  Look for "finished data load" in the log output before issuing requests.  

The application listens on localhost:8080, a postman file is included if needed. 

If the key has expired, it can be updated in the application.properties file. 

## Assumptions 

* Aggregations are all by UTC date not by local timezone, as illustrated in the example output in the exercise description. 
* Start and end parameters are not validated to be of the same significance as the interval granularity. This would be a nice addition but was not a requirement. 
* If no data exists for a timeframe, the timeframe will not be included in the results. 
* Voided items are excluded from all reporting.
* Error reporting responses for the API was not a requirement.  Some rudimentary error handling exists to report back parameter problems and errors in an error response body. 

