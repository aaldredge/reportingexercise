package com.aaldredge.reporting;

import com.aaldredge.reporting.dataload.loader.Loader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Runner to kick off load of data when application loads
 */
@Component
public class LoaderRunnerBean implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(ApplicationRunner.class);
    @Autowired
    Loader loader;

    @Override
    public void run(ApplicationArguments arg0) {
        log.info("starting data load");
        loader.loadData();
        log.info("finished data load");

    }
}
