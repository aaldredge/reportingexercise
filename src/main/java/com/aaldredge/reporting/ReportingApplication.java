package com.aaldredge.reporting;

import com.aaldredge.reporting.dataload.loader.Loader;
import com.aaldredge.reporting.dataload.loader.RestLoader;
import com.aaldredge.reporting.rest.service.ReportService;
import com.aaldredge.reporting.rest.service.impl.ReportServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Spring Boot application entry point
 */
@SpringBootApplication
public class ReportingApplication {

	private static final Logger log = LoggerFactory.getLogger(ReportingApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ReportingApplication.class, args);
	}


	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public ReportService reportService(){
		return new ReportServiceImpl();
	}

	@Bean
	public Loader loader(){
		return new RestLoader();
	}

}

