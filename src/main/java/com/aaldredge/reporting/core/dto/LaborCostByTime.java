package com.aaldredge.reporting.core.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public interface LaborCostByTime {

    //make Timestamp - workaround to "java.lang.IllegalArgumentException: Projection type must be an interface" as OffsetDateTime

    public Timestamp getStart();

    public Timestamp getEnd();

    public BigDecimal getPrice();
}
