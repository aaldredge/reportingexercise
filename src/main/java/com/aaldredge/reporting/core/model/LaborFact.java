package com.aaldredge.reporting.core.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Table(indexes = { @Index(name = "lf_business_id_idx", columnList="businessId", unique = false),
        @Index(name = "lf_hour_id_idx", columnList="hour", unique = false),
        @Index(name = "lf_employee_id_idx", columnList="employeeId", unique = false)})
@Entity
public class LaborFact {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    protected String id;

    protected String businessId;

    protected String employeeId;

    protected OffsetDateTime hour;

    protected BigDecimal cost;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public OffsetDateTime getHour() {
        return hour;
    }

    public void setHour(OffsetDateTime hour) {
        this.hour = hour;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }
}
