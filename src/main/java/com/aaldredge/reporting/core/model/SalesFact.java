package com.aaldredge.reporting.core.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Table(indexes = { @Index(name = "sf_business_id_idx", columnList="businessId", unique = false),
        @Index(name = "sf_employee_id_idx", columnList="employeeId", unique = false),
        @Index(name = "sf_hour_id_idx", columnList="hour", unique = false),
        @Index(name = "sf_check_id_idx", columnList="checkId", unique = false)})
@Entity
public class SalesFact {

    @Id
    protected String id;

    protected String employeeId;

    protected String checkId;

    protected String businessId;

    protected OffsetDateTime hour;

    protected BigDecimal cost;

    protected BigDecimal price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public OffsetDateTime getHour() {
        return hour;
    }

    public void setHour(OffsetDateTime hour) {
        this.hour = hour;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCheckId() {
        return checkId;
    }

    public void setCheckId(String checkId) {
        this.checkId = checkId;
    }
}
