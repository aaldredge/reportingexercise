package com.aaldredge.reporting.core.repository;

import com.aaldredge.reporting.core.model.EmployeeDimension;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeDimensionRepository extends JpaRepository<EmployeeDimension, String>{

}
