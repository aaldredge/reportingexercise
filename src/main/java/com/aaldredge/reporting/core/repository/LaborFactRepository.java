package com.aaldredge.reporting.core.repository;

import com.aaldredge.reporting.core.model.LaborFact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Repository
public interface LaborFactRepository extends JpaRepository<LaborFact, String> {

    @Query("select SUM(l.cost) " +
            "from LaborFact l  " +
            "where l.hour >= :start and l.hour < :end  and l.businessId = :businessId ")
    BigDecimal findLaborCostForTimeFrame(String businessId, OffsetDateTime start, OffsetDateTime end);

}
