package com.aaldredge.reporting.core.repository;

import com.aaldredge.reporting.core.dto.EmployeeSalesByTime;
import com.aaldredge.reporting.core.dto.FoodCostByTime;
import com.aaldredge.reporting.core.dto.LaborCostByTime;
import com.aaldredge.reporting.core.model.SalesFact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;

/**
 * Repository for sales facts
 * note: uses H2 specific native queries
 */
@Repository
public interface SalesFactRepository extends JpaRepository<SalesFact, String> {

    @Transactional
    @Modifying
    @Query("update SalesFact s set s.hour = :hour where s.businessId = :businessId and s.checkId = :checkId")
    void setHourByBusinessIdAndCheckId(OffsetDateTime hour, String businessId, String checkId);

    /*
    Ideally these would all return Stream<> but ran into an issue with the datasource closing that
    required more debugging and research than time allowed.
    */
    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd-HH'), 'yyyy-MM-dd-HH') as start, " +
            "DATEADD('HOUR', 1, PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd-HH'), 'yyyy-MM-dd-HH')) as end, " +
            "SUM(s.cost) as cost, SUM(s.price) as price " +
            "from SALES_FACT s where s.hour >= :start and s.hour < :end and s.business_id = :businessId " +
            "group by start, end " +
            "order by start"
    )
    List<FoodCostByTime> findAllFoodCostByHour(@Param("businessId") String businessId,
                                               @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);

    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd', null, 'UTC'), 'yyyy-MM-dd', null, 'UTC') as start, " +
            "DATEADD('DAY', 1, PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd', null, 'UTC'), 'yyyy-MM-dd', null, 'UTC')) as end, " +
            "SUM(s.cost) as cost, SUM(s.price) as price " +
            "from SALES_FACT s where s.hour >= :start and s.hour < :end and s.business_id = :businessId " +
            "group by start, end " +
            "order by start"
    )
    List<FoodCostByTime> findAllFoodCostByDay(@Param("businessId") String businessId,
                                               @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);

    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-w', null, 'UTC'), 'yyyy-w', null, 'UTC') as start, " +
            "DATEADD('WEEK', 1, PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-w', null, 'UTC'), 'yyyy-w', null, 'UTC')) as end, " +
            "SUM(s.cost) as cost, SUM(s.price) as price " +
            "from SALES_FACT s where s.hour >= :start and s.hour < :end and s.business_id = :businessId " +
            "group by start, end " +
            "order by start"
    )
    List<FoodCostByTime> findAllFoodCostByWeek(@Param("businessId") String businessId,
                                              @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);

    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM', null, 'UTC'), 'yyyy-MM', null, 'UTC') as start, " +
            "PARSEDATETIME(FORMATDATETIME(DATEADD('MONTH', 1, s.hour), 'yyyy-MM', null, 'UTC'), 'yyyy-MM', null, 'UTC')  as end, " +
            "SUM(s.cost) as cost, SUM(s.price) as price " +
            "from SALES_FACT s where s.hour >= :start and s.hour < :end and s.business_id = :businessId " +
            "group by start, end " +
            "order by start"
    )
    List<FoodCostByTime> findAllFoodCostByMonth(@Param("businessId") String businessId,
                                              @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);


    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd-HH'), 'yyyy-MM-dd-HH') as start, " +
            "DATEADD('HOUR', 1, PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd-HH'), 'yyyy-MM-dd-HH')) as end, " +
            "SUM(s.price) as price " +
            "from SALES_FACT s where s.hour >= :start and s.hour < :end  and s.business_id = :businessId " +
            "group by start, end " +
            "order by start")
    List<LaborCostByTime> findAllPriceByHour(@Param("businessId") String businessId,
                                             @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);

    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd', null, 'UTC'), 'yyyy-MM-dd', null, 'UTC') as start, " +
            "DATEADD('DAY', 1, PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd', null, 'UTC'), 'yyyy-MM-dd', null, 'UTC')) as end, " +
            "SUM(s.price) as price " +
            "from SALES_FACT s where s.hour >= :start and s.hour < :end  and s.business_id = :businessId " +
            "group by start, end " +
            "order by start")
    List<LaborCostByTime> findAllPriceByDay(@Param("businessId") String businessId,
                                             @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);

    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-w', null, 'UTC'), 'yyyy-w', null, 'UTC') as start, " +
            "DATEADD('WEEK', 1, PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-w', null, 'UTC'), 'yyyy-w', null, 'UTC')) as end, " +
            "SUM(s.price) as price " +
            "from SALES_FACT s where s.hour >= :start and s.hour < :end  and s.business_id = :businessId " +
            "group by start, end " +
            "order by start")
    List<LaborCostByTime> findAllPriceByWeek(@Param("businessId") String businessId,
                                             @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);

    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM', null, 'UTC'), 'yyyy-MM', null, 'UTC') as start, " +
            "PARSEDATETIME(FORMATDATETIME(DATEADD('MONTH', 1, s.hour), 'yyyy-MM', null, 'UTC'), 'yyyy-MM', null, 'UTC')  as end, " +
            "SUM(s.price) as price " +
            "from SALES_FACT s where s.hour >= :start and s.hour < :end  and s.business_id = :businessId " +
            "group by start, end " +
            "order by start")
    List<LaborCostByTime> findAllPriceByMonth(@Param("businessId") String businessId,
                                             @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);

    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd-HH'), 'yyyy-MM-dd-HH') as start, " +
            "DATEADD('HOUR', 1, PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd-HH'), 'yyyy-MM-dd-HH')) as end, " +
            "SUM(s.price) as price, CONCAT(MAX(e.first_name) , ' ', MAX(e.last_name)) as employeeName " +
            "from SALES_FACT s join EMPLOYEE_DIMENSION e on e.employee_id = s.employee_id " +
            "where s.hour >= :start and s.hour < :end  and s.business_id = :businessId " +
            "group by start, end, s.employee_id " +
            "order by start, s.employee_id")
    List<EmployeeSalesByTime> findAllEmployeeSalesByHour(@Param("businessId") String businessId,
                                                         @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);


    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd', null, 'UTC'), 'yyyy-MM-dd', null, 'UTC') as start, " +
            "DATEADD('DAY', 1, PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM-dd', null, 'UTC'), 'yyyy-MM-dd', null, 'UTC')) as end, " +
            "SUM(s.price) as price, CONCAT(MAX(e.first_name) , ' ', MAX(e.last_name)) as employeeName " +
            "from SALES_FACT s join EMPLOYEE_DIMENSION e on e.employee_id = s.employee_id " +
            "where s.hour >= :start and s.hour < :end  and s.business_id = :businessId " +
            "group by start, end, s.employee_id " +
            "order by start, s.employee_id")
    List<EmployeeSalesByTime> findAllEmployeeSalesByDay(@Param("businessId") String businessId,
                                                         @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);


    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-w', null, 'UTC'), 'yyyy-w', null, 'UTC') as start, " +
            "DATEADD('WEEK', 1, PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-w', null, 'UTC'), 'yyyy-w', null, 'UTC')) as end, " +
            "SUM(s.price) as price, CONCAT(MAX(e.first_name) , ' ', MAX(e.last_name)) as employeeName " +
            "from SALES_FACT s join EMPLOYEE_DIMENSION e on e.employee_id = s.employee_id " +
            "where s.hour >= :start and s.hour < :end  and s.business_id = :businessId " +
            "group by start, end, s.employee_id " +
            "order by start, s.employee_id")
    List<EmployeeSalesByTime> findAllEmployeeSalesByWeek(@Param("businessId") String businessId,
                                                         @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);


    @Transactional(readOnly = true)
    @Query(nativeQuery = true, value = "select " +
            "PARSEDATETIME(FORMATDATETIME(s.hour, 'yyyy-MM', null, 'UTC'), 'yyyy-MM', null, 'UTC') as start, " +
            "PARSEDATETIME(FORMATDATETIME(DATEADD('MONTH', 1, s.hour), 'yyyy-MM', null, 'UTC'), 'yyyy-MM', null, 'UTC')  as end, " +
            "SUM(s.price) as price, CONCAT(MAX(e.first_name) , ' ', MAX(e.last_name)) as employeeName " +
            "from SALES_FACT s join EMPLOYEE_DIMENSION e on e.employee_id = s.employee_id " +
            "where s.hour >= :start and s.hour < :end  and s.business_id = :businessId " +
            "group by start, end, s.employee_id " +
            "order by start, s.employee_id")
    List<EmployeeSalesByTime> findAllEmployeeSalesByMonth(@Param("businessId") String businessId,
                                                         @Param("start") OffsetDateTime start, @Param("end") OffsetDateTime end);

}
