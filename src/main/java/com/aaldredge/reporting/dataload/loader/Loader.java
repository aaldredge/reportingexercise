package com.aaldredge.reporting.dataload.loader;

/**
 * Loader interface for loading data into database
 */
public interface Loader {

    /**
     * Load the data
     */
    public void loadData();
}
