package com.aaldredge.reporting.dataload.loader;

import com.aaldredge.reporting.dataload.model.BaseResponse;;
import com.aaldredge.reporting.dataload.model.Check;
import com.aaldredge.reporting.dataload.model.CheckResponse;
import com.aaldredge.reporting.dataload.model.Employee;
import com.aaldredge.reporting.dataload.model.EmployeeResponse;
import com.aaldredge.reporting.dataload.model.LaborEntry;
import com.aaldredge.reporting.dataload.model.LaborEntryResponse;
import com.aaldredge.reporting.dataload.model.OrderedItem;
import com.aaldredge.reporting.dataload.model.OrderedItemResponse;
import com.aaldredge.reporting.core.model.EmployeeDimension;
import com.aaldredge.reporting.core.model.LaborFact;
import com.aaldredge.reporting.core.model.SalesFact;
import com.aaldredge.reporting.core.repository.EmployeeDimensionRepository;
import com.aaldredge.reporting.core.repository.LaborFactRepository;
import com.aaldredge.reporting.core.repository.SalesFactRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Implementation to load the data from the rest data sources
 */
public class RestLoader implements Loader {

    private static final Logger log = LoggerFactory.getLogger(RestLoader.class);

    private static final long DEFAULT_LIMIT = 500;

    @Value("${dataload.restLoader.baseUrl}")
    private String baseUrl;

    @Value("${dataload.restLoader.authKey}")
    private String authKey;

    @Autowired
    protected RestTemplate restTemplate;

    @Autowired
    protected EmployeeDimensionRepository employeeDimensionRepository;

    @Autowired
    protected LaborFactRepository laborFactRepository;

    @Autowired
    protected SalesFactRepository salesFactRepository;

    @Override
    public void loadData() {
        loadEmployeeData();
        loadLaborEntryData();
        loadCheckData();
    }

    protected void loadCheckData() {
        long orderedItemCount = 0;
        long orderedItemLoaded = 0;
        long offset = 0;
        boolean foundResults = false;  //to stop looping in case the counts are inaccurate from the service
        long voidedCount = 0;
        do {
            ResponseEntity<OrderedItemResponse> orderedItems = makeCall("/orderedItems",
                    OrderedItemResponse.class, DEFAULT_LIMIT, offset);
            log.debug(orderedItems.toString());
            foundResults = orderedItems.getBody() != null && orderedItems.getBody().getCount() > 0L;
            if (orderedItems.getBody() != null) {
                orderedItemCount = orderedItems.getBody().getCount();
                if (orderedItems.getBody().getData() != null) {
                    for (OrderedItem orderedItem : orderedItems.getBody().getData()) {
                        log.debug(orderedItem.toString());
                        if (!orderedItem.isVoided()) {
                            SalesFact salesFact = new SalesFact();
                            salesFact.setId(orderedItem.getId());
                            salesFact.setEmployeeId(orderedItem.getEmployeeId());
                            salesFact.setBusinessId(orderedItem.getBusinessId());
                            salesFact.setCost(orderedItem.getCost());
                            salesFact.setPrice(orderedItem.getPrice());
                            salesFact.setCheckId(orderedItem.getCheckId());
                            salesFactRepository.save(salesFact);
                        } else {
                            log.debug("skipped voided item :" + orderedItem.toString());
                            voidedCount++;
                        }
                        orderedItemLoaded++;
                    }
                }
            }
            log.info("loaded " + orderedItemLoaded + " of " + orderedItemCount + " orderedItems - void count " + voidedCount);
            offset = offset + DEFAULT_LIMIT;
        } while (orderedItemCount > orderedItemLoaded && foundResults);
        log.info("loaded " + orderedItemCount + " orderedItems - void count " + voidedCount);

        //now update them all with the dates from the checks
        long checkCount = 0;
        long checkLoaded = 0;
        offset = 0;
        foundResults = false;  //to stop looping in case the counts are inaccurate from the service
        do {
            ResponseEntity<CheckResponse> checks = makeCall("/checks",
                    CheckResponse.class, DEFAULT_LIMIT, offset);
            log.debug(checks.toString());
            foundResults = checks.getBody() != null && checks.getBody().getCount() > 0L;
            if (checks.getBody() != null) {
                checkCount = checks.getBody().getCount();
                if (checks.getBody().getData() != null) {
                    for (Check check : checks.getBody().getData()) {
                        log.debug(check.toString());
                        salesFactRepository.setHourByBusinessIdAndCheckId(
                                check.getClosedAt(), check.getBusinessId(), check.getId());
                        checkLoaded++;
                    }
                }
            }
            offset = offset + DEFAULT_LIMIT;
            log.info("loaded " + checkLoaded + " of " + checkCount + " checks");

        } while (checkCount > checkLoaded && foundResults);
        log.info("loaded " + checkCount + " checks");
    }

    protected void loadEmployeeData() {
        long employeeCount = 0;
        long employeesLoaded = 0;
        long offset = 0;
        boolean foundResults = false;  //to stop looping in case the counts are inaccurate from the service
        do {
            ResponseEntity<EmployeeResponse> employees = makeCall("/employees",
                    EmployeeResponse.class, DEFAULT_LIMIT, offset);
            log.debug(employees.toString());
            foundResults = employees.getBody() != null && employees.getBody().getCount() > 0L;
            if (employees.getBody() != null) {
                employeeCount = employees.getBody().getCount();
                if (employees.getBody().getData() != null) {
                    for (Employee employee : employees.getBody().getData()) {
                        log.debug(employee.toString());
                        EmployeeDimension employeeDimension = new EmployeeDimension();
                        employeeDimension.setEmployeeId(employee.getId());
                        employeeDimension.setBusinessId(employee.getBusinessId());
                        employeeDimension.setFirstName(employee.getFirstName());
                        employeeDimension.setLastName(employee.getLastName());
                        employeeDimensionRepository.save(employeeDimension);
                        employeesLoaded++;
                    }
                }
            }
            offset = offset + DEFAULT_LIMIT;
            log.info("loaded " + employeesLoaded + " of " + employeeCount + " employees");
        } while (employeeCount > employeesLoaded && foundResults);
        log.info("loaded " + employeeCount + " employees");
    }

    protected void loadLaborEntryData() {

        long laborEntryCount = 0;
        long laborEntryLoaded = 0;
        long offset = 0;
        boolean foundResults = false;  //to stop looping in case the counts are inaccurate from the service
        do {
            ResponseEntity<LaborEntryResponse> laborEntries = makeCall("/laborEntries",
                    LaborEntryResponse.class, DEFAULT_LIMIT, 0);
            log.debug(laborEntries.toString());
            foundResults = laborEntries.getBody() != null && laborEntries.getBody().getCount() > 0L;
            if (laborEntries.getBody() != null) {
                laborEntryCount = laborEntries.getBody().getCount();
                if (laborEntries.getBody().getData() != null) {
                    for (LaborEntry laborEntry : laborEntries.getBody().getData()) {
                        log.debug(laborEntry.toString());
                        /*
                        interested to find out the desired approach for this as nothing seems
                        elegant to flatten for easy querying
                         */
                        List<LaborFact> facts = new ArrayList<>();
                        OffsetDateTime time = laborEntry.getClockIn();
                        while (time.isBefore(laborEntry.getClockOut())) {
                            LaborFact laborFact = new LaborFact();
                            laborFact.setEmployeeId(laborEntry.getEmployeeId());
                            laborFact.setBusinessId(laborEntry.getBusinessId());
                            if (time.equals(laborEntry.getClockIn()) && time.getMinute() != 0) {
                                //need minutes for partial cost - I may be being too picky here
                                BigDecimal partialHour = new BigDecimal((60 - time.getMinute())/60);
                                laborFact.setCost(laborEntry.getPayRate().multiply(partialHour)
                                        .setScale(2, RoundingMode.HALF_UP));
                            } else if (time.plusHours(1).isAfter(laborEntry.getClockOut())) {
                                //need minutes for partial cost - I may be being too picky here
                                BigDecimal partialHour = new BigDecimal((laborEntry.getClockOut().getMinute())/60);
                                laborFact.setCost(laborEntry.getPayRate().multiply(partialHour)
                                        .setScale(2, RoundingMode.HALF_UP));
                            } else {
                                laborFact.setCost(laborEntry.getPayRate());
                            }
                            laborFact.setHour(time.withMinute(0).withSecond(0).withNano(0));
                            facts.add(laborFact);
                            time = time.withMinute(0).withSecond(0).withNano(0).plusHours(1);
                        }
                        laborFactRepository.saveAll(facts);
                        laborEntryLoaded++;
                    }
                }
            }
            offset = offset + DEFAULT_LIMIT;
            log.info("loaded " + laborEntryLoaded + " of " + laborEntryCount + " laborEntries");
        } while (laborEntryCount > laborEntryLoaded && foundResults);
        log.info("loaded " + laborEntryCount + " laborEntries");
    }

    protected <T extends BaseResponse> ResponseEntity<T> makeCall(String path,
                                            Class<T> responseType, long limit, long offset) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, authKey);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> request = new HttpEntity<String>(null, headers);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + path);
        if (limit > 0L) {
            builder = builder.queryParam("limit", limit);
        }
        if (offset > 0L) {
            builder = builder.queryParam("offset", offset);
        }
        ResponseEntity responseEntity = restTemplate.exchange( builder.toUriString(),
                HttpMethod.GET, request,
                responseType);
        return responseEntity;

    }
}
