package com.aaldredge.reporting.dataload.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class BaseData {
    protected String id;

    @JsonProperty("business_id")
    protected String businessId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}
