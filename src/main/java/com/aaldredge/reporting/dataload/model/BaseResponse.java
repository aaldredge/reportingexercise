package com.aaldredge.reporting.dataload.model;

import java.util.List;

public abstract class BaseResponse<T extends BaseData> {
    protected long count;

    protected List<T> data;

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "count=" + count +
                "data=" +
                (data != null?("["+data.size()+"]"):"[]") +
                '}';
    }
}
