package com.aaldredge.reporting.dataload.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LaborEntry extends BaseData {
    @JsonProperty("employee_id")
    protected String employeeId;

    protected String name;

    @JsonProperty("clock_in")
    protected OffsetDateTime clockIn;

    @JsonProperty("clock_out")
    protected OffsetDateTime clockOut;

    @JsonProperty("pay_rate")
    protected BigDecimal payRate;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OffsetDateTime getClockIn() {
        return clockIn;
    }

    public void setClockIn(OffsetDateTime clockIn) {
        this.clockIn = clockIn;
    }

    public OffsetDateTime getClockOut() {
        return clockOut;
    }

    public void setClockOut(OffsetDateTime clockOut) {
        this.clockOut = clockOut;
    }

    public BigDecimal getPayRate() {
        return payRate;
    }

    public void setPayRate(BigDecimal payRate) {
        this.payRate = payRate;
    }

    @Override
    public String toString() {
        return "LaborEntry{" +
                "employeeId='" + employeeId + '\'' +
                ", name='" + name + '\'' +
                ", clockIn=" + clockIn +
                ", clockOut=" + clockOut +
                ", payRate=" + payRate +
                ", id='" + id + '\'' +
                ", businessId='" + businessId + '\'' +
                '}';
    }
}
