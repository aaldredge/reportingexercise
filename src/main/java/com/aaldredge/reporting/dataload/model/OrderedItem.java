package com.aaldredge.reporting.dataload.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderedItem extends BaseData {

    @JsonProperty("employee_id")
    protected String employeeId;

    @JsonProperty("check_id")
    protected String checkId;

    protected BigDecimal cost;

    protected BigDecimal price;

    protected boolean voided;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getCheckId() {
        return checkId;
    }

    public void setCheckId(String checkId) {
        this.checkId = checkId;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isVoided() {
        return voided;
    }

    public void setVoided(boolean voided) {
        this.voided = voided;
    }

    @Override
    public String toString() {
        return "OrderedItem{" +
                "employeeId='" + employeeId + '\'' +
                ", checkId='" + checkId + '\'' +
                ", cost=" + cost +
                ", price=" + price +
                ", voided=" + voided +
                ", id='" + id + '\'' +
                ", businessId='" + businessId + '\'' +
                '}';
    }
}
