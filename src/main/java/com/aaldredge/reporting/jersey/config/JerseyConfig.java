package com.aaldredge.reporting.jersey.config;

import com.aaldredge.reporting.jersey.providers.OffsetDateTimeProvider;
import com.aaldredge.reporting.rest.controller.ReportingController;
import com.aaldredge.reporting.jersey.providers.exception.DefaultExceptionMapper;
import com.aaldredge.reporting.jersey.providers.exception.ParamExceptionMapper;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig{
    public JerseyConfig() {
        register(ReportingController.class);
        register(OffsetDateTimeProvider.class);
        register(DefaultExceptionMapper.class);
        register(ParamExceptionMapper.class);
    }
}
