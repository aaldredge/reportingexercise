package com.aaldredge.reporting.jersey.providers;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Param converter for ISO Date Time to OffsetDateTime
 */
@Provider
public class OffsetDateTimeProvider implements ParamConverterProvider {

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> aClass, Type type, Annotation[] annotations) {
        if (aClass.getName().equals(OffsetDateTime.class.getName())) {

            return new ParamConverter<T>() {

                @SuppressWarnings("unchecked")
                @Override
                public T fromString(String value) {
                    OffsetDateTime time = OffsetDateTime.parse(value, DateTimeFormatter.ISO_DATE_TIME);
                    return (T) time;
                }

                @Override
                public String toString(T time) {
                    return ((LocalDateTime) time).format(DateTimeFormatter.ISO_DATE_TIME);
                }
            };
        }
        return null;
    }
}
