package com.aaldredge.reporting.jersey.providers.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Default exception handler to return response bodies, would add more for specific exceptions before rollout
 */
@Provider
public class DefaultExceptionMapper implements ExceptionMapper<Exception> {
    private static final Logger log = LoggerFactory.getLogger(DefaultExceptionMapper.class);

    @Override
    public Response toResponse(Exception e) {
        log.info("Exception thrown", e);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setError(e.getMessage());
        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }

}
