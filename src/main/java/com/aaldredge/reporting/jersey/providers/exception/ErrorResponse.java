package com.aaldredge.reporting.jersey.providers.exception;

/**
 * DTO for error responses for REST calls
 */
public class ErrorResponse {
    protected String error;
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
