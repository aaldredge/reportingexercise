package com.aaldredge.reporting.jersey.providers.exception;

import org.glassfish.jersey.server.ParamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Exception handler for parameter problems
 * Ideally would clean up the messages, right now they show the java detail
 */
@Provider
public class ParamExceptionMapper implements ExceptionMapper<ParamException> {
    private static final Logger log = LoggerFactory.getLogger(ParamExceptionMapper.class);

    @Override
    public Response toResponse(ParamException e) {
        log.info("Exception thrown", e);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setError(e.getCause().getMessage());
        return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
    }

}
