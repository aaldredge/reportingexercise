package com.aaldredge.reporting.rest.controller;

import com.aaldredge.reporting.rest.model.ReportChunk;
import com.aaldredge.reporting.rest.model.ReportTimeInterval;
import com.aaldredge.reporting.rest.model.ReportType;
import com.aaldredge.reporting.rest.service.ReportService;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.util.Objects;
import java.util.stream.Stream;

@Service
@Path("/reporting")
public class ReportingController {
    private static final Logger log = LoggerFactory.getLogger(ReportingController.class);
    @Autowired
    protected ReportService reportService;

    /**
     * Method exposed for API requests for reporting
     * @param businessId
     * @param report
     * @param timeInterval
     * @param start
     * @param end
     * @return Response with requested data
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional(readOnly = true)
    public Response reporting(
            @QueryParam("business_id") String businessId,
            @QueryParam("report")ReportType report,
            @QueryParam("timeInterval") ReportTimeInterval timeInterval,
            @QueryParam("start") OffsetDateTime start,
            @QueryParam("end") OffsetDateTime end) {
        log.info("params are : " + businessId + " " + report + " " + timeInterval + " " + start + " " + end);
        Objects.requireNonNull(businessId, "business_id may not be null");
        Objects.requireNonNull(report, "report may not be null");
        Objects.requireNonNull(timeInterval, "timeInterval may not be null");
        Objects.requireNonNull(start, "start may not be null");
        Objects.requireNonNull(end, "end may not be null");

        final StreamingOutput streamingOutput = new JsonStreamingOutput(businessId, report,
                timeInterval, start, end, reportService);
        return Response.ok(streamingOutput).build();
    }

    /**
     * Provider of streaming output for responses
     */
    private static class JsonStreamingOutput implements StreamingOutput {

        private String businessId;
        private ReportType reportType;
        private ReportTimeInterval reportTimeInterval;
        private OffsetDateTime start;
        private OffsetDateTime end;
        private ReportService reportService;

        public JsonStreamingOutput(String businessId, ReportType reportType,
                                   ReportTimeInterval reportTimeInterval, OffsetDateTime start,
                                   OffsetDateTime end, ReportService reportService) {
            this.businessId = businessId;
            this.reportType = reportType;
            this.reportTimeInterval = reportTimeInterval;
            this.start = start;
            this.end = end;
            this.reportService = reportService;
        }

        @Override
        @Transactional(readOnly = true)
        public void write(final OutputStream outputStream) throws IOException, WebApplicationException {
            final ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            final JsonFactory jsonFactory = objectMapper.getFactory();
            try (final JsonGenerator jsonGenerator = jsonFactory.createGenerator(outputStream)) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeStringField("report", reportType.toString());
                jsonGenerator.writeStringField("timeInterval", reportTimeInterval.toString());

                jsonGenerator.writeFieldName("data");
                jsonGenerator.writeStartArray();
                try (Stream stream = getReportData(businessId, reportType, reportTimeInterval, start, end)) {
                    stream.forEach(item -> {
                        try {
                            jsonGenerator.writeObject(item);
                            jsonGenerator.flush();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
                    jsonGenerator.writeEndArray();
                }
                jsonGenerator.writeEndObject();
            }
        }

        @Transactional(readOnly = true)
        private Stream<? extends ReportChunk> getReportData(String businessId, ReportType reportType,
                                                  ReportTimeInterval timeInterval,
                                                  OffsetDateTime start,
                                                  OffsetDateTime end) {
            switch (reportType) {
                case EGS:
                    return reportService.getEmployeeGrossSales(businessId, timeInterval, start, end);
                case FCP:
                    return reportService.getFoodCostPercentage(businessId, timeInterval, start, end);
                case LCP:
                    return reportService.getLaborCostPercentage(businessId, timeInterval, start, end);
                default:
                    throw new UnsupportedOperationException("reportType " + reportType + " not implemented.");
            }
        }

    }
}