package com.aaldredge.reporting.rest.model;

public enum ReportTimeInterval {
    hour, day, week, month;
}
