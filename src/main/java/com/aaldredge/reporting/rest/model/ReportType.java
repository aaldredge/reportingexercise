package com.aaldredge.reporting.rest.model;

public enum ReportType {
    LCP, FCP, EGS;
}
