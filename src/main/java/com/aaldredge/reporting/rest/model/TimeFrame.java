package com.aaldredge.reporting.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.time.OffsetDateTime;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeFrame", propOrder = {
        "start",
        "end"
})
public class TimeFrame {

    public TimeFrame(){
    }

    public TimeFrame(OffsetDateTime start, OffsetDateTime end) {
        this.start = start;
        this.end = end;
    }

    protected OffsetDateTime start;
    protected OffsetDateTime end;

    public OffsetDateTime getStart() {
        return start;
    }

    public void setStart(OffsetDateTime start) {
        this.start = start;
    }

    public OffsetDateTime getEnd() {
        return end;
    }

    public void setEnd(OffsetDateTime end) {
        this.end = end;
    }
}
