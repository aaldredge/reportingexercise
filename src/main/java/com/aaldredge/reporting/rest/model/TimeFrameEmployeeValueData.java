package com.aaldredge.reporting.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeFrameEmployeeValueData", propOrder = {
        "employee"
})
public class TimeFrameEmployeeValueData extends TimeFrameValueData {

    public TimeFrameEmployeeValueData() {
    }

    public TimeFrameEmployeeValueData(TimeFrame timeFrame, BigDecimal value, String employee) {
        super(timeFrame, value);
        this.employee = employee;
    }

    protected String employee;

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }
}
