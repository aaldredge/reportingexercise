package com.aaldredge.reporting.rest.model;

import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;

@XmlType(name = "TimeFrameValueData", propOrder = {
        "value"
})
public class TimeFrameValueData extends ReportChunk {

    public TimeFrameValueData() {

    }

    public TimeFrameValueData(TimeFrame timeFrame, BigDecimal value) {
        this.timeFrame = timeFrame;
        this.value = value;
    }

    protected TimeFrame timeFrame;

    protected BigDecimal value;

    public TimeFrame getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(TimeFrame timeFrame) {
        this.timeFrame = timeFrame;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}

