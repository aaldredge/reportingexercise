package com.aaldredge.reporting.rest.service;

import com.aaldredge.reporting.rest.model.ReportChunk;
import com.aaldredge.reporting.rest.model.ReportTimeInterval;
import com.aaldredge.reporting.rest.model.ReportType;
import com.aaldredge.reporting.rest.model.TimeFrameEmployeeValueData;
import com.aaldredge.reporting.rest.model.TimeFrameValueData;

import java.time.OffsetDateTime;
import java.util.stream.Stream;

public interface ReportService {

    /**
     * Get the Labor Cost Percentage Report
     * @param businessId
     * @param timeInterval
     * @param start
     * @param end
     * @return stream of Labor Cost Percentage data
     */
    public Stream<TimeFrameValueData> getLaborCostPercentage(String businessId,
                                                             ReportTimeInterval timeInterval,
                                                             OffsetDateTime start,
                                                             OffsetDateTime end);

    /**
     * Get the Food Cost Percentage Report
     * @param businessId
     * @param timeInterval
     * @param start
     * @param end
     * @return stream of Food Cost Percentage data
     */
    public Stream<TimeFrameValueData> getFoodCostPercentage(String businessId,
                                                          ReportTimeInterval timeInterval,
                                                          OffsetDateTime start,
                                                          OffsetDateTime end);

    /**
     * Get the Employee Gross Sales Report
     * @param businessId
     * @param timeInterval
     * @param start
     * @param end
     * @return stream of employee gross sales
     */
    public Stream<TimeFrameEmployeeValueData> getEmployeeGrossSales(String businessId,
                                                                  ReportTimeInterval timeInterval,
                                                                  OffsetDateTime start,
                                                                  OffsetDateTime end);

}
