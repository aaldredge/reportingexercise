package com.aaldredge.reporting.rest.service.impl;

import com.aaldredge.reporting.core.dto.EmployeeSalesByTime;
import com.aaldredge.reporting.core.dto.FoodCostByTime;
import com.aaldredge.reporting.core.dto.LaborCostByTime;
import com.aaldredge.reporting.core.repository.LaborFactRepository;
import com.aaldredge.reporting.core.repository.SalesFactRepository;
import com.aaldredge.reporting.dataload.loader.RestLoader;
import com.aaldredge.reporting.rest.model.ReportTimeInterval;
import com.aaldredge.reporting.rest.model.TimeFrame;
import com.aaldredge.reporting.rest.model.TimeFrameEmployeeValueData;
import com.aaldredge.reporting.rest.model.TimeFrameValueData;
import com.aaldredge.reporting.rest.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class ReportServiceImpl implements ReportService {
    private static final Logger log = LoggerFactory.getLogger(RestLoader.class);

    @Autowired
    protected LaborFactRepository laborFactRepository;

    @Autowired
    protected SalesFactRepository salesFactRepository;

    @Override
    @Transactional(readOnly = true)
    public Stream<TimeFrameValueData> getLaborCostPercentage(String businessId, ReportTimeInterval timeInterval,
                                                             OffsetDateTime start, OffsetDateTime end) {
        checkArguments(businessId, timeInterval, start, end);
        List<LaborCostByTime> resultList = null;
        switch (timeInterval) {
            case hour:
                resultList = salesFactRepository.findAllPriceByHour(businessId, start, end);
                break;
            case day:
                resultList = salesFactRepository.findAllPriceByDay(businessId, start, end);
                break;
            case week:
                resultList = salesFactRepository.findAllPriceByWeek(businessId, start, end);
                break;
            case month:
                resultList = salesFactRepository.findAllPriceByMonth(businessId, start, end);
                break;
            default:
                throw new IllegalArgumentException("need to implement!");
        }
        return resultList.stream()
                .map(priceByTime -> {

                    OffsetDateTime startFrame = OffsetDateTime.ofInstant(Instant.ofEpochMilli(priceByTime.getStart().getTime()),
                            ZoneId.of("UTC"));
                    OffsetDateTime endFrame =
                            OffsetDateTime.ofInstant(Instant.ofEpochMilli(priceByTime.getEnd().getTime()),
                                    ZoneId.of("UTC"));
                    BigDecimal percentAsWholeNumber = null;

                    if (BigDecimal.ZERO.compareTo(priceByTime.getPrice()) != 0) { //don't want divide by zero error

                        //get the labor cost for the period
                        BigDecimal laborCost = laborFactRepository.findLaborCostForTimeFrame(businessId,
                                startFrame, endFrame);
                        log.info("businessId " + businessId + " timeframe " + startFrame + " " +
                                endFrame + " laborCost " + laborCost);
                        if (laborCost == null) {
                            percentAsWholeNumber = BigDecimal.ZERO;
                        } else {
                            percentAsWholeNumber = laborCost
                                    .divide(priceByTime.getPrice(), 4, RoundingMode.HALF_UP)
                                    .multiply(new BigDecimal(100));
                        }
                    }
                    TimeFrameValueData timeFrameValueData =
                            new TimeFrameValueData(new TimeFrame(startFrame, endFrame),
                                    percentAsWholeNumber);
                    return timeFrameValueData;

                });

    }

    @Override
    @Transactional(readOnly = true)
    public Stream<TimeFrameValueData> getFoodCostPercentage(String businessId, ReportTimeInterval timeInterval,
                                                            OffsetDateTime start, OffsetDateTime end) {
        checkArguments(businessId, timeInterval, start, end);
        List<FoodCostByTime> results = null;

        switch (timeInterval) {
            case hour:
                results = salesFactRepository.findAllFoodCostByHour(businessId, start, end);
                break;
            case day:
                results = salesFactRepository.findAllFoodCostByDay(businessId, start, end);
                break;
            case week:
                results = salesFactRepository.findAllFoodCostByWeek(businessId, start, end);
                break;
            case month:
                results = salesFactRepository.findAllFoodCostByMonth(businessId, start, end);
                break;
            default:
                throw new IllegalArgumentException("need to implement!");
        }
        return results.stream()
                .map(foodCostByTime -> {
                    BigDecimal percentAsWholeNumber = null;
                    if (BigDecimal.ZERO.compareTo(foodCostByTime.getPrice()) != 0) { //don't want divide by zero error
                        percentAsWholeNumber = foodCostByTime.getCost()
                                .divide(foodCostByTime.getPrice(), 4, RoundingMode.HALF_UP)
                                .multiply(new BigDecimal(100));
                    }
                    log.debug("percent is " + percentAsWholeNumber);
                    log.debug("start " + foodCostByTime.getStart());
                    log.debug(" end " + foodCostByTime.getEnd());
                    TimeFrameValueData timeFrameValueData =
                        new TimeFrameValueData(new TimeFrame(
                            OffsetDateTime.ofInstant(Instant.ofEpochMilli(foodCostByTime.getStart().getTime()),
                                    ZoneId.of("UTC")),
                            OffsetDateTime.ofInstant(Instant.ofEpochMilli(foodCostByTime.getEnd().getTime()),
                                    ZoneId.of("UTC"))),
                            percentAsWholeNumber);
                    return timeFrameValueData;
                });

    }

    @Override
    @Transactional(readOnly = true)
    public Stream<TimeFrameEmployeeValueData> getEmployeeGrossSales(String businessId,
                                        ReportTimeInterval timeInterval, OffsetDateTime start, OffsetDateTime end) {
        checkArguments(businessId, timeInterval, start, end);
        List<EmployeeSalesByTime> results = null;
        switch (timeInterval) {
            case hour:
                results = salesFactRepository.findAllEmployeeSalesByHour(businessId, start, end);
                break;
            case day:
                results = salesFactRepository.findAllEmployeeSalesByDay(businessId, start, end);
                break;
            case week:
                results = salesFactRepository.findAllEmployeeSalesByWeek(businessId, start, end);
                break;
            case month:
                results = salesFactRepository.findAllEmployeeSalesByMonth(businessId, start, end);
                break;
            default:
                throw new IllegalArgumentException("need to implement!");
        }
        return results.stream()
                .map(employeeSalesByTime -> {
                    TimeFrameEmployeeValueData timeFrameEmployeeValueData =
                        new TimeFrameEmployeeValueData(
                            new TimeFrame(
                                OffsetDateTime.ofInstant(Instant.ofEpochMilli(employeeSalesByTime.getStart().getTime()),
                                        ZoneId.of("UTC")),
                                OffsetDateTime.ofInstant(Instant.ofEpochMilli(employeeSalesByTime.getEnd().getTime()),
                                        ZoneId.of("UTC"))),
                            employeeSalesByTime.getPrice(), employeeSalesByTime.getEmployeeName());
                    return timeFrameEmployeeValueData;
                });

    }

    private void checkArguments(String businessId, ReportTimeInterval timeInterval, OffsetDateTime start, OffsetDateTime end) {
        Objects.requireNonNull(businessId, "businessId may not be null");
        Objects.requireNonNull(timeInterval, "timeInterval may not be null");
        Objects.requireNonNull(start, "start may not be null");
        Objects.requireNonNull(end, "end may not be null");

    }

}
